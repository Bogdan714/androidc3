package com.example.lenovo.timer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public static final String KEY = "key";
    @BindView(R.id.seconds)
    EditText seconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.start)
    public void onClick(){
        String timeString = seconds.getText().toString();
        if(timeString != "" || timeString.length() < 10) {
            Intent intent = new Intent(this, TimerActivity.class);
            intent.putExtra(KEY, Integer.parseInt(timeString));
            startActivity(intent);
        }
    }
}
