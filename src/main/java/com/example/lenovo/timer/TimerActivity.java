package com.example.lenovo.timer;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimerActivity extends AppCompatActivity {
    int time = 0;
    @BindView(R.id.seconds)
    TextView textView;

    @BindView(R.id.action)
    Button action;

    Thread thread;
    boolean suspended = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            time = getIntent().getIntExtra(MainActivity.KEY, 0);
        }
        @SuppressLint("HandlerLeak") final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what > 0) {
                    textView.setText(msg.what + "");
                } else {
                    textView.setText("Time is over");
                }
            }
        };
        Runnable runnable = new Runnable() {
            public void run() {
                while (time >= 0) {

                    handler.sendEmptyMessageDelayed(time, 1000);
                    try {
                        Thread.sleep(999);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (suspended) {
                        handler.removeMessages(time);
                    }else {
                        time--;
                    }

                }
            }
        };
        thread = new Thread(runnable);
        thread.start();
    }

    @OnClick(R.id.action)
    public void onClick() {
        if (suspended) {
            action.setText("Pause");
        } else {
            action.setText("Resume");
        }
        suspended = !suspended;

    }

    @OnClick(R.id.Back)
    public void onBackClicked() {
        finish();
    }

}
